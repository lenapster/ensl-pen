#!/bin/bash
echo "==Starting"
virsh start openWisk-core
virsh start invoker0
#virsh start TP-invoker1
ipcore=""
while [ "$ipcore" == "" ]; do
	echo "==Checking for openwisk-core IP address"
	ipcore=$(virsh net-dhcp-leases default | grep core | awk -F " " '{print $5}' | awk -F "/" '{print $1}')
	sleep 5
done
pinv0=""
while [ "$ipinv0" == "" ]; do
        echo "==Checking for invoker0 IP address"
	ipinv0=$(virsh net-dhcp-leases default | grep invoker0 | awk -F " " '{print $5}' | awk -F "/" '{print $1}')	
        sleep 5
done
#sleep 2
#ipinv1=$(virsh net-dhcp-leases default | grep invoker1 | awk -F " " '{print $5}' | awk -F "/" '{print $1}')
echo $ipcore
echo $ipinv0
echo "==Creating kubernetes cluster"
var=$(ssh root@$ipcore /home/core/script.sh)
token=$(echo $var | grep kubeadm | awk -F " " '{print $5}')
echo $token
disco=$(echo $var | grep kubeadm | awk -F " " '{print $7}')
echo $disco
ssh root@$ipinv0 /home/invoker0/script.sh
ssh root@$ipinv0 kubeadm join $ipcore:6443 --token $token --discovery-token-ca-cert-hash $disco
#ssh root@$ipinv1 /home/invoker1/script.sh
#ssh root@$ipinv1 kubeadm join $ipcore:6443 --token $token --discovery-token-ca-cert-hash $disco
echo "==Installation of Openwhisk"
ssh root@$ipcore /home/core/script_2.sh
