kubectl label nodes invoker0 invoker1 openwhisk-role=invoker
helm init
kubectl create clusterrolebinding tiller-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default
kubectl get pods -n kube-system
sleep 180
cd /home/core/incubator-openwhisk-deploy-kube
helm install ./helm/openwhisk --namespace=openwhisk --name=owdev -f mycluster.yaml
