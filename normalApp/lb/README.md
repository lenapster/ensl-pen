### Run RSLB:

```bash
cd <path to normalApp>/lb
make clean && make
./rslb lb.cfg
```

### Run backends:

#### Compile backend:

```bash
gcc -o be be.c
```

Open one terminal for each backend.

#### Terminal 1:

```bash
./be 8081
```

#### Terminal 2:

```bash
./be 8082
```
...

Make sure that the backend addresses are listed in the RSLB configuration file.

To test the load balancer:

 * HTML documents: http://localhost:9000/website/index.html

Check the RSLB log output to see that the load balancer balances the loads in a round-robin way.
