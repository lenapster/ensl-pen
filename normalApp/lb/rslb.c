#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>

#include "epollinterface.h"
#include "logging.h"
#include "server_socket.h"
#include "queue.h"

struct configuration {
    char listen_port[MAX_PORT_BUFF];
    struct queue_root* backend_list;
};

char * trim_space(char *str) {
    char *end;
    /* skip leading whitespace */
    while (isspace(*str)) {
        str = str + 1;
    }
    /* remove trailing whitespace */
    end = str + strlen(str) - 1;
    while (end > str && isspace(*end)) {
        end = end - 1;
    }
    *(end+1) = '\0';
    return str;
}

struct configuration* read_config_file(char* file_path) {
    FILE *fp = fopen(file_path, "r");
    if (fp == NULL) {
      rsp_log("Unable to open config file: %s\n", file_path);
      exit(1);
    }

    struct configuration* config = (struct configuration*) malloc(sizeof(struct configuration));
    struct queue_root* backend_list = (struct queue_root*) malloc(sizeof(struct queue_root));
    init_queue(backend_list);

    char buff[4096] = {0};
    char server_address[MAX_ADDR_BUFF] = {0};

    while(fgets(buff, sizeof(buff), fp) != NULL) {
      char* trimmed_buff = trim_space(buff);

      char* token = trim_space(strtok(trimmed_buff, "="));
      if (token) {
        if (strcmp(token, "listenPort") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              strncpy(config->listen_port, trim_space(token), sizeof(config->listen_port));
              rsp_log("listenPort = %s", config->listen_port);
            }
        } else if (strcmp(token, "backends") == 0) {

          token = trim_space(strtok(NULL, ","));
          while (token) {
              strncpy(server_address, token, sizeof(server_address));

              struct queue_item* backend_entry = (struct queue_item*)malloc(sizeof(struct queue_item));
              struct server_endpoint* ep = (struct server_endpoint*)
                                                          malloc(sizeof(struct server_endpoint));

              char *pos = strstr(server_address, ":");
              if (pos != NULL) {
                *pos = '\0';
                strncpy(ep->address, trim_space(server_address), MAX_ADDR_BUFF);
                strncpy(ep->port, trim_space(pos+1), MAX_PORT_BUFF);
              } else
                strncpy(ep->address, trim_space(server_address), MAX_ADDR_BUFF);

              if (strlen(ep->port) == 0)
                strncpy(ep->port, "80", MAX_PORT_BUFF);

              rsp_log("Adding backend %s:%s", ep->address, ep->port);

              backend_entry->data = ep;
              enqueue(backend_list, backend_entry);

              token = strtok(NULL, ",");
          }
        }
      }
      memset(buff, 0, sizeof(buff));
    }

    if (is_empty(backend_list)) {
        rsp_log("No backend found in the configuration file");
        exit(1);
    }

    config->backend_list = backend_list;

    fclose(fp);

    return config;
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
        fprintf(stderr,
                "Usage: %s <config_file>\n",
                argv[0]);
        exit(1);
    }

    struct configuration* rslb_config = read_config_file(argv[1]);

    /*
     * A SIGPIPE is sent to a process if it tried to write to a socket
     * that had been shutdown for writing or isn't connected anymore.
     * To avoid that the program ends in this case, just ignore this signal.
     */
    signal(SIGPIPE, SIG_IGN);

    epoll_init();

    /*
     * Create a socket listening to incoming connections and
     * register it to epoll.
     */
    create_server_socket_handler(rslb_config->listen_port, rslb_config->backend_list);

    rsp_log("Started. Listening on port %s", rslb_config->listen_port);

    /*
     * A never-ending loop: wait for an epoll event on the registred fds and
     * call the appropriate handler.
     */
    epoll_do_reactor_loop();

    return 0;
}

