/* 
 * tiny.c - a minimal HTTP server that serves static and
 *          dynamic content with the GET method. Neither 
 *          robust, secure, nor modular. Use for instructional
 *          purposes only.
 *          Dave O'Hallaron, Carnegie Mellon
 *
 *          usage: tiny <port>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFSIZE 1024
#define MAXERRS 16

enum return_code_e
{
    RET_OK,
    RET_ERR
};

extern char **environ; /* the environment */

/*
 * error - wrapper for perror used for bad syscalls
 */
static void error(const char *msg) {
    perror(msg);
    exit(1);
}

/*
 * cerror - returns an error message to the client
 */
static void cerror(FILE *stream, const char *cause, const char *errno, 
	    const char *shortmsg, const char *longmsg) {
    fprintf(stream, "HTTP/1.1 %s %s\n", errno, shortmsg);
    fprintf(stream, "Content-type: text/html\n");
    fprintf(stream, "\n");
    fprintf(stream, "<html><title>Tiny Error</title>");
    fprintf(stream, "<body bgcolor=""ffffff"">\n");
    fprintf(stream, "%s: %s\n", errno, shortmsg);
    fprintf(stream, "<p>%s: %s\n", longmsg, cause);
    fprintf(stream, "<hr><em>The Tiny Web server</em>\n");
}

static int open_server(unsigned short portno, int queuelen) {
    struct sockaddr_in serveraddr; /* server's addr */
    int optval = 1;                /* flag value for setsockopt */

    /* open socket descriptor */
    int parentfd = socket(AF_INET, SOCK_STREAM, 0);
    if (parentfd < 0) 
        error("ERROR opening socket");

    /* allows us to restart server immediately */
    setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR, 
           (const void *)&optval , sizeof(int));

    /* bind port to socket */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(portno);
    if (bind(parentfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
        error("ERROR on binding");

    /* get us ready to accept connection requests */
    if (listen(parentfd, queuelen) < 0) 
      error("ERROR on listen");

    return parentfd;
}

static void serve_static_content(const char *filename, size_t content_size,
        FILE *stream) {
    int fd;
    char *p;
    char filetype[BUFSIZE]; /* path derived from uri */

    if (strstr(filename, ".html"))
        strcpy(filetype, "text/html");
    else if (strstr(filename, ".css"))
        strcpy(filetype, "text/css");
    else if (strstr(filename, ".gif"))
        strcpy(filetype, "image/gif");
    else if (strstr(filename, ".jpg"))
        strcpy(filetype, "image/jpg");
    else 
        strcpy(filetype, "text/plain");

    /* print response header */
    fprintf(stream, "HTTP/1.1 200 OK\n");
    fprintf(stream, "Server: Tiny Web Server\n");
    fprintf(stream, "Content-length: %lu\n", content_size);
    fprintf(stream, "Content-type: %s\n", filetype);
    fprintf(stream, "\r\n"); 
    fflush(stream);

    /* Use mmap to return arbitrary-sized response body */
    fd = open(filename, O_RDONLY);
    p = mmap(0, content_size, PROT_READ, MAP_PRIVATE, fd, 0);
    fwrite(p, 1, content_size, stream);
    munmap(p, content_size);
    close(fd);
}

static enum return_code_e serve_dynamic_content(const char *filename,
        FILE *stream, int st_mode, int childfd, const char *cgiargs) {
    int pid; /* process id from fork */

    /* make sure file is a regular executable file */
    if (!(S_IFREG & st_mode) || !(S_IXUSR & st_mode)) {
        cerror(stream, filename, "403", "Forbidden", 
        "You are not allowed to access this item");
        return RET_ERR;
    }

    /* a real server would set other CGI environ vars as well*/
    setenv("QUERY_STRING", cgiargs, 1); 

    /* print first part of response header */
    fprintf(stream, "HTTP/1.1 200 OK\n");
    fprintf(stream, "Server: Tiny Web Server\n");
    fprintf(stream, "\r\n");
    fflush(stream);

    /* create and run the child CGI process so that all child
    output to stdout and stderr goes back to the client via the
    childfd socket descriptor */
    pid = fork();
    if (pid < 0)
        error("ERROR in fork");
    else if (pid > 0) { /* parent process */
        int wait_status;
        wait(&wait_status);
    }
    else { /* child  process*/
        char *argv[1] = {NULL};
        close(0); /* close stdin */
        dup2(childfd, 1); /* map socket to stdout */
        dup2(childfd, 2); /* map socket to stderr */
        if (execve(filename, argv, environ) < 0)
            error("ERROR in execve");
    }

    return RET_OK;
}

static void parse_http(FILE *stream, char *method, char *uri, char *version) {
    char buf[BUFSIZE];
    
    fgets(buf, BUFSIZE, stream);
    printf("%s", buf);
    sscanf(buf, "%s %s %s\n", method, uri, version);
}

static enum return_code_e serve_http(FILE *stream, const char *method, const char *uri, int childfd) {
    char buf[BUFSIZE];
    char filename[BUFSIZE]; /* path derived from uri */
    char cgiargs[BUFSIZE];  /* cgi argument list */
    int is_static;          /* static request? */
    struct stat sbuf;       /* file status */

    /* tiny only supports the GET method */
    if (strcasecmp(method, "GET")) {
        cerror(stream, method, "501", "Not Implemented", 
           "Tiny does not implement this method");
        return RET_ERR;
    }

    /* read (and ignore) the HTTP headers */
    do {
        fgets(buf, BUFSIZE, stream);
        printf("%s", buf);
    } while(strcmp(buf, "\r\n"));

    /* parse the uri [crufty] */
    if (!strstr(uri, "cgi-bin")) { /* static content */
        is_static = 1;
        strcpy(cgiargs, "");
        strcpy(filename, ".");
        strcat(filename, uri);
        if (uri[strlen(uri)-1] == '/') 
            strcat(filename, "index.html");
    } else { /* dynamic content */
        char *p = index(uri, '?');
        is_static = 0;
        if (p) {
            strcpy(cgiargs, p+1);
            *p = '\0';
        } else
            strcpy(cgiargs, "");
        strcpy(filename, ".");
        strcat(filename, uri);
    }

    /* make sure the file exists */
    if (stat(filename, &sbuf) < 0) {
        cerror(stream, filename, "404", "Not found", 
           "Tiny couldn't find this file");
        return RET_ERR;
    }

    /* serve static content */
    if (is_static)
        serve_static_content(filename, sbuf.st_size, stream);

    /* serve dynamic content */
    else
        return serve_dynamic_content(filename, stream, sbuf.st_mode, childfd, cgiargs);

    return RET_OK;
}

int main(int argc, char **argv) {

    /* variables for connection management */
    int parentfd; /* parent socket */
    int portno;   /* port to listen on */

    /* check command line args */
    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }

    portno = atoi(argv[1]);
    parentfd = open_server(portno, 5); /* allow 5 requests to queue up */

    /* 
     * main loop: wait for a connection request, parse HTTP,
     * serve requested content, close connection.
     */
    while (1) {

        /* variables for connection management */
        struct sockaddr_in clientaddr;      /* client addr */
        int childfd;                        /* child socket */
        socklen_t clientlen = sizeof(clientaddr); /* byte size of client's address */
#if 0
        struct hostent *hostp;              /* client host info */
        char *hostaddrp;                    /* dotted decimal host addr string */
#endif

        /* variables for connection I/O */
        FILE *stream;          /* stream version of childfd */
        char method[BUFSIZE];  /* request method */
        char uri[BUFSIZE];     /* request uri */
        char version[BUFSIZE]; /* request version */

        /* wait for a connection request */
        childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
        if (childfd < 0) 
            error("ERROR on accept");
        
#if 0
        /* determine who sent the message */
        hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, 
                  sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        if (hostp == NULL)
          error("ERROR on gethostbyaddr");
        hostaddrp = inet_ntoa(clientaddr.sin_addr);
        if (hostaddrp == NULL)
          error("ERROR on inet_ntoa\n");
#endif
        
        /* open the child socket descriptor as a stream */
        if ((stream = fdopen(childfd, "r+")) == NULL)
            error("ERROR on fdopen");

        /* get the HTTP request line */
        parse_http(stream, method, uri, version);

        /* serve the HTTP request */
        serve_http(stream, method, uri, childfd);

        /* clean up */
        fclose(stream);
        close(childfd);

    }

    return EXIT_SUCCESS;
}

