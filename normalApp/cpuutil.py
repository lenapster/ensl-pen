#!/usr/bin/env python3

from time import sleep
from os import system

prev_total = 0
prev_idle = 0

cpu_stat_file = '/proc/stat'

try:
    while True:
        with open(cpu_stat_file) as fp:
            cpu_stat = fp.readline().split()[1:]
        cpu_stat = [int(x) for x in cpu_stat]

        total = sum(cpu_stat[0:8])
        idle = sum(cpu_stat[3:5])

        diff_total = total - prev_total
        diff_idle = idle - prev_idle
        cpu_usage = diff_total - diff_idle

        if diff_total != 0:
            cpu_percentage = (cpu_usage / diff_total) * 100

        print("{0:.2f}".format(cpu_percentage))

        prev_total = total
        prev_idle = idle

        sleep(1)
except KeyboardInterrupt:
    print("Done")
