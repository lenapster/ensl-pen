#ifndef RSLB_LOGGING_H
#define RSLB_LOGGING_H
//For logging
extern void rsp_log(char* format, ...);
extern void rsp_log_error(char* message);

#endif
