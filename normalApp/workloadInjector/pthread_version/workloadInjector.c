#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <unistd.h> 
#include <pthread.h> 
#include <time.h>
#include <unistd.h> 

#include "logging.h"
#include "request_sender.h"

#define MAX_ADDR_BUFF   250
#define MAX_PORT_BUFF   10
#define MAX_NUMBER_BUFF   10
#define MAX_NUMBER_CLIENTS   1000
#define MAX_URL_BUFF  500

//To contact the entry point of the system under test: the loadbalancer 
struct server_endpoint {
    char address[MAX_ADDR_BUFF];
    char port[MAX_PORT_BUFF];
};

//For retrieving configuration parameters of the workload injector
struct workloadInjectorConfiguration {
    struct server_endpoint* loadBalancerAddress;
    int numberOfSimulatedClients;//Number of clients to simulate
    int maxNumberOfRequestsPerClient;//Number of requests sent by each client
    int delayBetweeenTwoRequestsInMsec;//Delay between two requests
    int numberOfHTMLPagesThatCanBeRequested;//Number of html pages in nomalApp BE
    int numberOfScriptsThatCanBeRequested;//Number of script files in nomalApp BE
};

//We use threads instead of processes to simulate a client.
//Therefore pthread_create primitive is used instead of fork()
struct clientThreadData {
  struct workloadInjectorConfiguration* cfg; //shared resource
  int local_id;
};

char * trim_space(char *str) {
    char *end;
    /* skip leading whitespace */
    while (isspace(*str)) {
        str = str + 1;
    }
    /* remove trailing whitespace */
    end = str + strlen(str) - 1;
    while (end > str && isspace(*end)) {
        end = end - 1;
    }
    *(end+1) = '\0';
    return str;
}

//Read the workload injector configuration file in order to retrieve configuration parameters
struct workloadInjectorConfiguration* read_config_file(char* file_path) {
    FILE *fp = fopen(file_path, "r");
    if (fp == NULL) {
      rsp_log("Unable to open config file: %s\n", file_path);
      exit(1);
    }

    struct workloadInjectorConfiguration* config = (struct workloadInjectorConfiguration*) malloc(sizeof(struct workloadInjectorConfiguration));

    char buff[4096] = {0};
    char lb_address[MAX_ADDR_BUFF] = {0};
    char nb[MAX_NUMBER_BUFF] = {0};

    while(fgets(buff, sizeof(buff), fp) != NULL) {
      char* trimmed_buff = trim_space(buff);

      char* token = trim_space(strtok(trimmed_buff, "="));
      if (token) {
        if (strcmp(token, "numberOfSimulatedClients") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              memset(nb, 0, sizeof nb);
              strncpy(nb, trim_space(token), sizeof(nb));
              config->numberOfSimulatedClients = atoi(nb);
              rsp_log("Number of simulated clients = %d", config->numberOfSimulatedClients);
            }
        }else if (strcmp(token, "maxNumberOfRequestsPerClient") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              memset(nb, 0, sizeof nb);
              strncpy(nb, trim_space(token), sizeof(nb));
              config->maxNumberOfRequestsPerClient = atoi(nb);
              rsp_log("Maximum number of requests sent by each client = %d", config->maxNumberOfRequestsPerClient);
            }
        }else if (strcmp(token, "delayBetweeenTwoRequestsInMsec") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              memset(nb, 0, sizeof nb);
              strncpy(nb, trim_space(token), sizeof(nb));
              config->delayBetweeenTwoRequestsInMsec = atoi(nb);
              rsp_log("Delay between two requests = %d ms", config->delayBetweeenTwoRequestsInMsec);
            }
        }else if (strcmp(token, "numberOfHTMLPagesThatCanBeRequested") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              memset(nb, 0, sizeof nb);
              strncpy(nb, trim_space(token), sizeof(nb));
              config->numberOfHTMLPagesThatCanBeRequested = atoi(nb);
              rsp_log("Number of pages that can be requested = %d", config->numberOfHTMLPagesThatCanBeRequested);
            }
        } else if (strcmp(token, "numberOfScriptsThatCanBeRequested") == 0) {
            token = strtok(NULL, "=");
            if (token != NULL) {
              memset(nb, 0, sizeof nb);
              strncpy(nb, trim_space(token), sizeof(nb));
              config->numberOfScriptsThatCanBeRequested = atoi(nb);
              rsp_log("Number of scripts that can be requested = %d", config->numberOfScriptsThatCanBeRequested);
            }
        } else if (strcmp(token, "loadBalancerAddress") == 0) {

          token = strtok(NULL, "=");
         
          strncpy(lb_address, token, sizeof(lb_address));

          struct server_endpoint* ep = (struct server_endpoint*)
                                                      malloc(sizeof(struct server_endpoint));

          char *pos = strstr(lb_address, ":");
          if (pos != NULL) {
            *pos = '\0';
            strncpy(ep->address, trim_space(lb_address), MAX_ADDR_BUFF);
            strncpy(ep->port, trim_space(pos+1), MAX_PORT_BUFF);
          } else
            strncpy(ep->address, trim_space(lb_address), MAX_ADDR_BUFF);

          if (strlen(ep->port) == 0)
            strncpy(ep->port, "80", MAX_PORT_BUFF);

          rsp_log("LB address = %s, and port = %s", ep->address, ep->port);
          config->loadBalancerAddress = ep;          
        }
      }
      memset(buff, 0, sizeof(buff));
    }

    fclose(fp);

    return config;
}

/*The function that each thread which simulates a client runs.
This is used by pthread_create.
pthread_create creates threads, instead of processes as fork().
Threads share the same address space, unlike processes.

*/
void *aClientSimulator(void *vargp) 
{ 
    struct clientThreadData* data;//For storing data from vargp. See pthread_create below.
    struct workloadInjectorConfiguration* config;
    int rand_page_or_script;//To randomly select between page and script
    int rand_PageOrScriptNumber;//To randomly select page or script number
    char request_path[MAX_URL_BUFF] = "";
    char hostname[MAX_URL_BUFF] = "";
    
    data= (struct clientThreadData*)vargp;
    config = data->cfg; 
    rsp_log("Client-%d -> Started",data->local_id);
    for (int i = 0; i < config->maxNumberOfRequestsPerClient; i++) {
      rand_PageOrScriptNumber = rand() % config->numberOfHTMLPagesThatCanBeRequested + 1;
      rand_page_or_script = rand() % 2; // 0 == page , 1  == script
      
      snprintf(hostname, MAX_URL_BUFF, "%s", config->loadBalancerAddress->address);
      if (rand_page_or_script){
        snprintf(request_path, MAX_URL_BUFF, "/website/page%d.html", rand_PageOrScriptNumber);
      } else {
        snprintf(request_path, MAX_URL_BUFF, "/cgi-bin/script%d.sh", rand_PageOrScriptNumber);
      }
       rsp_log("Client-%d -> sending request %s",data->local_id, request_path);
      //Send a request
      send_request(hostname, request_path, atoi(config->loadBalancerAddress->port), data->local_id);
      //Delay
      usleep(config->delayBetweeenTwoRequestsInMsec * 1000);
    }
    rsp_log("Client-%d -> Finished",data->local_id);
    return NULL;
} 
  
//Entry point of the workload injector
int main(int argc, char* argv[]){

    pthread_t tid[MAX_NUMBER_CLIENTS];
    struct clientThreadData datas[MAX_NUMBER_CLIENTS];
    int i; 
    struct workloadInjectorConfiguration* clients_config;
    
    printf("\nAn example of a workload injector for a web application.\n\n");
     if (argc != 2) {
        fprintf(stderr,
                "Usage: %s <config_file>\n Tips: You can use the workloadInjector.cfg as your default config file\n",
                argv[0]);
        exit(1);
    }
    //Get configuration parameters from a configuration file
    clients_config = read_config_file(argv[1]);
    //Cannot simulate more than MAX_NUMBER_CLIENTS
    if(clients_config->numberOfSimulatedClients > MAX_NUMBER_CLIENTS){
         fprintf(stderr, "Cannot simulate more than %d clients. Change MAX_NUMBER_CLIENTS \n", MAX_NUMBER_CLIENTS);
         exit(1);
    }
    rsp_log("-----------------------Start simulation------------------------");
    //Start simulator clients. A client is a thread, not a process. I change to thread in order to make you learn this usefull API pthread_create.
    //It is quite simple to use. man pthread_create for the documentation.
    for (i = 0; i < clients_config->numberOfSimulatedClients; i++) {
      datas[i].local_id = i;
      datas[i].cfg = clients_config;
      pthread_create(&tid[i], NULL, aClientSimulator, (void *)&datas[i] ); 
    }
    //Wait the end of all clients
    for (i = 0; i < clients_config->numberOfSimulatedClients; i++) {
      pthread_join(tid[i], NULL);  
    }
    rsp_log("-----------------------End simulation------------------------");
    return 0;
}
