#include "request_sender.h"
#define MAX_REQUEST_LEN 500

/*
Requests 'path' from the webserver 'hostname:lb_port'.
'thread_id' is the thread id of the requester.
*/
int send_request(char* hostname, char* path, unsigned short lb_port, int thread_id) {
    char buffer[BUFSIZ];
    char request[MAX_REQUEST_LEN];
    char request_template[] = "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n";//Template of a HTTP request
    struct protoent *protoent;  
    in_addr_t in_addr;
    int request_len;
    int socket_file_descriptor;
    ssize_t nbytes_total_sent, nbytes_last;
    struct hostent *hostent;
    struct sockaddr_in sockaddr_in;

    //Build a HTTP request (like the navigator) according to the HTTP protocol.
    request_len = snprintf(request, MAX_REQUEST_LEN, request_template, path,  hostname);
    rsp_log("Client-%d -> #######\n%s#######\n",thread_id, request);
    if (request_len >= MAX_REQUEST_LEN) {
	rsp_log("Client-%d -> error: request length large: %d\n",thread_id, request_len);
        exit(EXIT_FAILURE);
    }
    /* Build the socket. */
    protoent = getprotobyname("tcp");
    if (protoent == NULL) {
        rsp_log("Client-%d -> error: getprotobyname.\n",thread_id);
        exit(EXIT_FAILURE);
    }
    socket_file_descriptor = socket(AF_INET, SOCK_STREAM, protoent->p_proto);
    if (socket_file_descriptor == -1) {
        rsp_log("Client-%d -> error: socket.\n",thread_id);
        exit(EXIT_FAILURE);
    }
    /* Build the address of the target server (LB). */
    hostent = gethostbyname(hostname);
    if (hostent == NULL) {
        rsp_log("Client-%d -> error: gethostbyname(\"%s\").\n",thread_id, hostname);
        exit(EXIT_FAILURE);
    }
    in_addr = inet_addr(inet_ntoa(*(struct in_addr*)*(hostent->h_addr_list)));
    if (in_addr == (in_addr_t)-1) {
        rsp_log("Client-%d -> error: inet_addr(\"%s\").\n",thread_id, *(hostent->h_addr_list));
        exit(EXIT_FAILURE);
    }
    sockaddr_in.sin_addr.s_addr = in_addr;
    sockaddr_in.sin_family = AF_INET;
    sockaddr_in.sin_port = htons(lb_port);
    /* Actually connect. */
    if (connect(socket_file_descriptor, (struct sockaddr*)&sockaddr_in, sizeof(sockaddr_in)) == -1) {
        perror("connect");
        exit(EXIT_FAILURE);
    }
    /* Send HTTP request. */
    nbytes_total_sent = 0;
    while (nbytes_total_sent < request_len) {
        nbytes_last = write(socket_file_descriptor, request + nbytes_total_sent, request_len - nbytes_total_sent);
        if (nbytes_last == -1) {
            rsp_log("Client-%d -> error: write.\n",thread_id);
            exit(EXIT_FAILURE);
        }
        nbytes_total_sent += nbytes_last;
    }

    /* Read the response. */
    while ((nbytes_total_sent = read(socket_file_descriptor, buffer, BUFSIZ)) > 0) {
	/*
	Uncomment the next 'write(STDOUT_FILENO, buffer, nbytes_total_sent);' to see the answer sent by the server.
	But comment it when performing experimentations because no need to display the result.
	*/
        //write(STDOUT_FILENO, buffer, nbytes_total_sent);
    }
    if (nbytes_total_sent == -1) {
        rsp_log("Client-%d -> error: read.\n",thread_id);
        exit(EXIT_FAILURE);
    }

    close(socket_file_descriptor);
    return 0;
}
