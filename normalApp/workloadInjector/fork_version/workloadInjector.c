#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_DOC_NUM 0

/*
Build 'struct addrinfo', which is used by the socket
*/
struct addrinfo *get_host_info(char* host, char* port) {
  int r;
  struct addrinfo hints, *getaddrinfo_res;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if ((r = getaddrinfo(host, port, &hints, &getaddrinfo_res))) {
    fprintf(stderr, "client[%d]: getaddrinfo() %s\n",
                        getpid(), gai_strerror(r));
    return NULL;
  }

  return getaddrinfo_res;
}

/*
Connection to the target system under test: the LB
*/
int establish_connection(struct addrinfo *info) {
  if (info == NULL) return -1;

  int clientfd;
  for (;info != NULL; info = info->ai_next) {
    if ((clientfd = socket(info->ai_family,
                           info->ai_socktype,
                           info->ai_protocol)) < 0) {
      fprintf(stderr, "client[%d]: socket() %s\n",
                          getpid(), strerror(errno));
      continue;
    }

    if (connect(clientfd, info->ai_addr, info->ai_addrlen) < 0) {
      close(clientfd);
      fprintf(stderr, "client[%d]: connect() %s\n",
                          getpid(), strerror(errno));
      continue;
    }

    freeaddrinfo(info);
    return clientfd;
  }

  freeaddrinfo(info);
  return -1;
}

/*
Build a GET method, according to the HTTP protocol syntax
*/
int GET(int clientfd, char *path) {
  char req[1000] = {0};
  sprintf(req,
    "GET %s HTTP/1.1\r\nUser-Agent: Workload Injector 1.0\r\n\r\n",
    path);
  return send(clientfd, req, strlen(req), 0);
}

/*
Build the request path
*/
void get_request_path(char *path, size_t len) {
  if (rand() % 2)
    snprintf(path, len, "/website/page%d.html",
                  rand() % (MAX_DOC_NUM+1));
  else
    snprintf(path, len, "/cgi-bin/script%d.sh",
                  rand() % (MAX_DOC_NUM+1));
}

double timdiff_us(struct timespec* start, struct timespec* end) {
  struct timespec diff;

  if ((end->tv_nsec - start->tv_nsec) < 0) {
    diff.tv_sec = end->tv_sec - start->tv_sec - 1;
    diff.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
  } else {
    diff.tv_sec = end->tv_sec - start->tv_sec;
    diff.tv_nsec = end->tv_nsec - start->tv_nsec;
  }

  return (diff.tv_sec * 1000000 + diff.tv_nsec / 1000);
}

/*
The entry point of the workload injector
*/
int main(int argc, char **argv) {
  int c, r, clientfd;
  int delay_ms, delay_s;
  int num_clients, num_requests;
  char buf[1024] = {0};
  pid_t* clients;
  pid_t pid;

  if (argc != 6) {
    fprintf(stderr,
      "USAGE: ./workloadInjector <server address> <port> <# of clients> <# of requests> <delay in ms>\n");
    return 1;
  }

  pid = getpid();

  printf("wi[%d]: server %s, port %s\n", pid, argv[1], argv[2]);

  num_clients = atoi(argv[3]);
  printf("wi[%d]: # of clients = %d\n", pid, num_clients);

  clients = (pid_t*)malloc(sizeof(pid_t) * num_clients);

  num_requests = atoi(argv[4]);
  printf("wi[%d]: # of requests = %d\n", pid, num_requests);

  delay_ms = atoi(argv[5]);
  printf("wi[%d]: delay = %d ms\n", pid, delay_ms);

  delay_s = 0;
  if (delay_ms >= 1000) {
    delay_s = delay_ms / 1000;
    delay_ms = delay_ms - delay_s * 1000;
  }

  struct timespec sleep_time = {delay_s, delay_ms * 1000000};

  pid_t client_pid;
  for (c = 0; c < num_clients; c++) {
    client_pid = fork();
    switch(client_pid) {
      case 0:
        goto child_process;
      case -1:
        fprintf(stderr, "wi[%d]: Cannot create child process #%d\n",
                            pid, c);
        exit(1);
      default:
        clients[c] = client_pid;
        continue;
    }
  }

  printf("wi[%d]: %d client(s) created\n", pid, num_clients);

  for (c = 0; c < num_clients; c++)
    waitpid(clients[c], NULL, 0);

  free(clients);

  return 0;

//The child code. The simulation of a client behavior is done here.
child_process:

  free(clients);

  char path[100] = {0};
  struct timespec start, end;
  double res_time_us;

  srand(time(NULL));

  pid = getpid();

  for (r = 0; r < num_requests; r++) {
    clientfd = establish_connection(get_host_info(argv[1], argv[2]));
    if (clientfd == -1) {
      fprintf(stderr, "client[%d]: failed to connect to %s:%s\n",
                          pid, argv[1], argv[2]);
      continue;
    }

    get_request_path(path, sizeof(path));
    if (GET(clientfd, path) == -1) {
      fprintf(stderr, "client[%d]: error while sending request %d -> %s\n",
                          pid, r, path);
      close(clientfd);
      continue;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    while (recv(clientfd, buf, sizeof(buf), 0) > 0) ;
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

    res_time_us = timdiff_us(&start, &end);

    printf("client[%d]: request %d -> %s, done in %f (us)\n",
                  pid, r, path, res_time_us);

    close(clientfd);

    nanosleep(&sleep_time, NULL);
  }

  return 0;
}
