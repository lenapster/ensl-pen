echo "Array_size Baseline"
#The size of an element in the array is 8 bytes. Therefore, 128*8 equal 1KiB
for size in 1 2 3 4 5 6 7 8 32 40 50 100 150 200 250 256 300 310 350 400 450 500 1000 1500 4000 5000 6000 7000 8000 8192 8292 8392 8492 8592 8692 8792 8892 8992 9192
do
	sizeInKiB=$(( 128*size ))
	gcc -DBASELINE -DSIZE=$sizeInKiB -o cache-eval cache-eval.c 2>/dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	baseline=$(perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs |sed 's/ //g')

	gcc -DSIZE=$sizeInKiB -o cache-eval cache-eval.c 2>/dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs > /dev/null
	notBaseline=$(perf stat -e cycles -C 7 taskset 0x80  ./cache-eval  2>&1  |grep cycles |cut -d'c' -f1 |xargs |sed 's/ //g')

	#echo "***"$baseline"---"$notBaseline"***"
	cycles=$(( notBaseline - baseline ))
	cycles=$(( cycles/100000000 ))

	echo $size" "$cycles
done
	
