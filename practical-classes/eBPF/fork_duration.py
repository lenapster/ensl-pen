#!/usr/bin/python

'''
This program monitors all the fork calls. You can improve this by providing a <pid> parameter.
This allows the program to monitor fork calls from a specific processs only.
'''

from __future__ import print_function
from bcc import BPF
from bcc.utils import printb

# load BPF program
b = BPF(src_file = "fork_duration.c")
b.attach_uprobe(name="c", sym="fork", fn_name="do_entry")
b.attach_uretprobe(name="c", sym="fork", fn_name="do_return")

# header
print("Tracing... Hit Ctrl-C to end.")

# format output
def print_event(cpu, data, size):
    output = b["output"].event(data)
    duration_usec = output.fork_duration / 1000
    printb("%s: fork duration %s usec" % (output.c, duration_usec))

# loop with callback to print_event
b["output"].open_perf_buffer(print_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()