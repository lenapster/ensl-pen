#include <uapi/linux/ptrace.h>
#include <linux/sched.h>

struct key_t {
	char c[TASK_COMM_LEN];
};

struct data_t {
	char c[TASK_COMM_LEN];
	u64 fork_duration;
};

BPF_PERF_OUTPUT(output);

// Hash table storing the start time of a fork call.
// The key in the hash table is the program name.
BPF_HASH(start, struct key_t);

int do_entry(struct pt_regs *ctx)
{
	struct key_t key = {};
	u64 ts, *val;

	// Get program name
	bpf_get_current_comm(&key.c, sizeof(key.c));

	// Get timestamp
	ts = bpf_ktime_get_ns();

	// Store the pair: <program-name> -> timestamp
	start.update(&key, &ts);
	return 0;
}

int do_return(struct pt_regs *ctx)
{
	struct key_t key = {};
	u64 *tsp, delta;
	struct data_t data = {};

	bpf_get_current_comm(&key.c, sizeof(key.c));
	tsp = start.lookup(&key);

	if (tsp != 0) {
		// Calculate the fork duration
		delta = bpf_ktime_get_ns() - *tsp;

		// Copy key.c to data.c
		bpf_probe_read(&data.c, sizeof(data.c), (void*)key.c);
		data.fork_duration = delta;

		// Submit the data to user
		output.perf_submit(ctx, &data, sizeof(data));
		start.delete(&key);
	}

	return 0;
}
