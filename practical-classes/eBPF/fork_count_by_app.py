#!/usr/bin/python

from __future__ import print_function
from bcc import BPF
from bcc.utils import printb
from time import sleep

# load BPF program
b = BPF(text="""
#include <linux/sched.h>

struct key_t {
    char c[TASK_COMM_LEN];
};
BPF_HASH(counts, struct key_t);

int count(struct pt_regs *ctx) {
    struct key_t key = {};
    u64 zero = 0, *val;

    bpf_get_current_comm(&key.c, sizeof(key.c));
    val = counts.lookup_or_init(&key, &zero);
    (*val)++;
    return 0;
};
""")
b.attach_kprobe(event="_do_fork", fn_name="count")
#b.attach_uprobe(name="c", sym="fork", fn_name="count")

# header
print("Tracing _do_fork()... Hit Ctrl-C to end and show the results.")

# sleep until Ctrl-C
try:
    sleep(99999999)
except KeyboardInterrupt:
    pass

# print output
print("%10s %s" % ("COUNT", "CMD"))
counts = b.get_table("counts")
for k, v in sorted(counts.items(), key=lambda counts: counts[1].value):
    printb(b"%10d \"%s\"" % (v.value, k.c))
